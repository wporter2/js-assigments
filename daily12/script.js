function getRandomInt(max) {
    return Math.floor(Math.random() * Math.floor(max));
}


document.getElementById("enter").addEventListener("click", function() {
    /* Get horse names and values */
    const hider = document.getElementsByClassName("instructions");
    for (let guy of hider) {
        guy.style.display = "none";
    }
    const shower = document.getElementsByClassName("flex-container");
    for (let guy of shower) {
        guy.classList.add("shower");
    }
});

document.getElementById("submit").addEventListener("click", function() {
    /* Get horse names and values */
    const horseSkills = [];
    const horseNames = []
    horseNames.push(document.getElementById("Horse1").value);
    const skills = document.getElementsByName("optionsRadios");
    for (let i = 0; i < 3; i++) {
        if (skills[i].checked) {
            horseSkills.push(i + 1);
        }
    };

    horseNames.push(document.getElementById("Horse2").value);
    const skills2 = document.getElementsByName("optionsRadios second");
    for (let i = 0; i < 3; i++) {
        if (skills2[i].checked) {
            horseSkills.push(i + 1);
        }
    };

    horseNames.push(document.getElementById("Horse3").value);
    const skills3 = document.getElementsByName("optionsRadios three")
    for (let i = 0; i < 3; i++) {
        if (skills3[i].checked) {
            horseSkills.push(i + 1);
        }
    };

    horseNames.push(document.getElementById("Horse4").value);
    const skills4 = document.getElementsByName("optionsRadios four");
    for (let i = 0; i < 3; i++) {
        if (skills4[i].checked) {
            horseSkills.push(i + 1);
        }
    };

    horseNames.push(document.getElementById("Horse5").value);
    const skills5 = document.getElementsByName("optionsRadios five");
    for (let i = 0; i < 3; i++) {
        if (skills5[i].checked) {
            horseSkills.push(i + 1);
        }
    };

    const skillSum = horseSkills.reduce(function(a, b) {
        return a + b;
    }, 0);
    let winner = 1 + getRandomInt(skillSum - 1);
    let curr_index = 0;
    let helper = 1
    for (let i = 0; i <= horseSkills.length; i++) {
        winner -= horseSkills[i];
        if (winner <= 0) {
            curr_index = i;
            break;
        }
    }

    let winningHorse = horseNames[curr_index];
    if (winningHorse == undefined) {
        winningHorse = horseNames[0];
    }
    document.getElementById("winnerwinner").innerText = winningHorse;
    const shower = document.getElementsByClassName("winners");
    console.log(shower);
    for (let guy of shower) {
        guy.classList.add("shower");
    }
    console.log(horseNames[curr_index]);
});