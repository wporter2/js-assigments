


document.getElementById("submit").addEventListener("click", function () {
    console.log("clicked!");

    /* First API call */
    var countryCode = swapKeyValue(codes[0]);
    console.log(countryCode);
    const name = document.getElementById('name').value;
    nationality(name, countryCode);
});


function nationality(name, countryCode) {
    nationalityReq = `https://api.nationalize.io/?name=${name}`;
    fetch(nationalityReq)
        .then(response => response.json())
        .then(data => {
            let nationality;
            if (data['country'].length === 0) {
                nationality = 'US';
            }
            else {
                nationality = data['country'][0]['country_id']
            }
            console.log(data);
            console.log(countryCode[nationality]);
            nationality = countryCode[nationality];
            var text = `${name}, your nationality is most likely ${nationality}`;
            document.getElementById('national').innerText = text;
            currency(nationality);
        })
}

function currency(country) {
    let country_curr = curr[0][country];
    console.log(country_curr);
    currReq = 'https://api.exchangerate-api.com/v4/latest/USD';
    fetch(currReq).then(response => response.json()).then(data => {
        let exchangeRate = data['rates'][country_curr]
        if (typeof(exchangeRate) === 'undefined') {
            exchangeRate = '(error: could not locate currency)';
        }
        var text = `1 USD is worth ${exchangeRate} ${country_curr}`;
        document.getElementById('exchange').innerText = text;
    })
}





function swapKeyValue(j) {
    let code_res = {};
    for (let key in j) {
        code_res[j[key]] = key;
    }
    console.log("AHHH");
    return code_res;
}