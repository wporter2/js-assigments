import unittest
import requests
import json

class TestReset(unittest.TestCase):

    SITE_URL = 'http://student04.cse.nd.edu:51095' # replace with your port id
    print("Testing for server: " + SITE_URL)
    RESET_URL = SITE_URL + '/reset/'
    MOVIES_URL = SITE_URL + '/movies/'

    def is_json(self, resp):
        try:
            json.loads(resp)
            return True
        except ValueError:
            return False

    def test_put_reset_index(self):
        m = {}
        r = requests.put(self.RESET_URL)

        r = requests.get(self.MOVIES_URL)
        self.assertTrue(self.is_json(r.content.decode()))
        resp = json.loads(r.content.decode())
        self.assertEqual(resp['result'], 'success')

        testmovie = {}
        movies = resp['movies']
        for movie in movies:
            if movie['id'] == 32:
                testmovie = movie

        self.assertEqual(testmovie['title'], 'Twelve Monkeys (1995)')
        self.assertEqual(testmovie['genres'], 'Drama|Sci-Fi')


    def test_put_reset_key(self):
        movie_id = 95
        r = requests.put(self.RESET_URL + str(movie_id))
        self.assertTrue(self.is_json(r.content.decode('utf-8')))
        resp = json.loads(r.content.decode('utf-8'))
        self.assertEqual(resp['result'], 'success')

        r2 = requests.get(self.MOVIES_URL + str(movie_id))
        self.assertTrue(self.is_json(r2.content.decode('utf-8')))
        resp2 = json.loads(r2.content.decode('utf-8'))
        self.assertEqual(resp2['result'], 'success')
        self.assertEqual(resp2['title'], 'Broken Arrow (1996)')
        self.assertEqual(resp2['genres'], 'Action|Thriller')
	
	

if __name__ == "__main__":
    unittest.main()

