import cherrypy
import re, json
from movies_library import _movie_database

# cmurph29 conor murphy
# wporter2 billy porter

class RatingsController(object):

	def __init__(self, mdb=None):
		if mdb is None:
			self.mdb = _movie_database()
			self.mdb.load_ratings('ratings.dat')
		else:
			self.mdb = mdb


	def GET_KEY(self, movie_id):
			'''when GET request for /ratings/movie_id comes in, then we respond with json string'''
			output = {'result': 'success'}
			movie_id = int(movie_id)

			try:
					rating = self.mdb.get_rating(movie_id)
					if rating is not None:
							output['rating'] = rating
					else:
							output['result'] = 'error'
							output['message'] = 'movie not found'
					output['movie_id'] = movie_id
			except Exception as ex:
					output['result'] = 'error'
					output['message'] = str(ex)

			return json.dumps(output)

