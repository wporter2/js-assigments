function getServerAddress(){
    option = document.getElementById("select-server-address").value;
    return option;
}

function getPortNumber(){
    port = document.getElementById("input-port-number").value;
    return port;
}

function getProtocol(){
    protocols = document.getElementsByName("bsr-radios");
    for(let elem of protocols){
        if(elem.checked){
            return elem.value;
        }
    }
}
function useKey(){
    key = document.getElementById("checkbox-use-key").checked;
    return key;
}
function getKey(){
    key = document.getElementById("input-key").value;
    return key;
}

function getMessageBody(){
    body = document.getElementById("text-message-body").value;
    return body;
}

function formatMovie(movie){
    return `${movie["title"]} belongs to the genres: ${movie["genres"]}`
}

async function sendRequest(address, port, protocol, key, body){
    const init = {
        method: protocol,
        mode: "cors"
    }
    if(protocol != "GET"){
        init.body = body;
    }
    if(address == "localhost"){
        address = "http://" + address;
    }
    const uri = address + (port ? ":" + port : "") + "/movies/" + (useKey() ? key : "");

    return fetch(uri, init);
}

const submit = document.getElementById("send-button");
submit.addEventListener('click', function(){
    let address = getServerAddress();
    let port = getPortNumber();
    let protocol = getProtocol();
    let key = getKey();
    let body = getMessageBody();

    sendRequest(address, port, protocol, key, body)
        .then(response => response.json())
        .then(data => {
            answer = document.getElementById("answer-label");
            answer.innerHTML = JSON.stringify(data);
            if(protocol == 'GET'){
                if(useKey()){
                    answer.innerHTML += "<br/>" + formatMovie(data);
                } else {
                    for(let movie of data["movies"]){
                        answer.innerHTML += "<br/>" + formatMovie(movie);
                    }
                }
            }
        });
})